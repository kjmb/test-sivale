import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutPrincipalComponent } from './vistas/layout-principal/layout-principal.component';
import { HeaderComponent } from './componentes/header/header.component';
import { FooterComponent } from './componentes/footer/footer.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { SkeletonModule } from 'primeng/skeleton';
import { MenubarModule } from 'primeng/menubar';
import { SidebarModule } from 'primeng/sidebar';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { PanelModule } from 'primeng/panel';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonModule } from 'primeng/button';
import { ListaPokemonComponent } from './vistas/lista-pokemon/lista-pokemon.component';
import { ChipModule } from 'primeng/chip';
import { FieldsetModule } from 'primeng/fieldset';
import { HttpClientModule } from '@angular/common/http';
import { DetallePokemonComponent } from './vistas/detalle-pokemon/detalle-pokemon.component';
import { DialogModule } from 'primeng/dialog';
import { CardModule } from 'primeng/card';
import { TableModule } from 'primeng/table';

@NgModule({
  declarations: [
    AppComponent,
    LayoutPrincipalComponent,
    HeaderComponent,
    FooterComponent,
    ListaPokemonComponent,
    DetallePokemonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxSpinnerModule,
    SkeletonModule,
    MenubarModule,
    SidebarModule,
    PanelModule,
    BrowserAnimationsModule,
    ButtonModule,
    OverlayPanelModule,
    ChipModule,
    FieldsetModule,
    HttpClientModule,
    DialogModule,
    CardModule,
    TableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
