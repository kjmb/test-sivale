import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutPrincipalComponent } from './vistas/layout-principal/layout-principal.component';
import { ListaPokemonComponent } from './vistas/lista-pokemon/lista-pokemon.component';

const routes: Routes = [
  {path:'pokemons',component: LayoutPrincipalComponent, children:[
    {path: '', component: ListaPokemonComponent},

  ]},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
