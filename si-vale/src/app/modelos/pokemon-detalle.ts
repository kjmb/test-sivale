export interface PokemonDetalle {
    abilities: Abilities[];
    forms: Form[];
    name: string;

}

export interface Abilities {
    ability: Ability;
    is_hidden: boolean;
    slot: number;
}

export interface Ability {
    name: string;
    url: string;
    
}

export interface Form {
    name: string;
    url: string;
}