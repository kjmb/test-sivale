import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { PokemonResponse } from '../modelos/pokemon-response';
import { PokemonDetalle } from '../modelos/pokemon-detalle';
@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  urlPokemonList = 'https://pokeapi.co/api/v2/pokemon/'
  headerDict = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
  }

  constructor( private http: HttpClient) { }

  obtenerListaPokemon(url: string):Observable<PokemonResponse>{
    return this.http.get<PokemonResponse>(url, {
      headers: new HttpHeaders(this.headerDict),
    }); 
  }

  obtenerPokemon(url: string):Observable<PokemonDetalle>{
    return this.http.get<PokemonDetalle>(url, {
      headers: new HttpHeaders(this.headerDict),
    }); 
  }
}
