import { Component, Input } from '@angular/core';
import { PokemonDetalle } from 'src/app/modelos/pokemon-detalle';
import { PokemonService } from 'src/app/servicios/pokemon.service';

@Component({
  selector: 'app-detalle-pokemon',
  templateUrl: './detalle-pokemon.component.html',
  styleUrls: ['./detalle-pokemon.component.css']
})
export class DetallePokemonComponent {
  @Input() pokemon:  PokemonDetalle = {
    abilities : [],
    forms: [],
    name: '',
  }
  detalle: PokemonDetalle = {
    abilities : [],
    forms: [],
    name: '',
  }
  visible: boolean = false;
  nombre: string = ""

  constructor( ) {
    console.log(this.pokemon)
    this.nombre = this.pokemon.name
  }

  ngOnInit() {
    this.nombre = this.pokemon.name
  }

  showDialog() {
    this.visible = true;
  }

}
