import { Component, OnInit, ViewChild } from '@angular/core';
import { PokemonItem, PokemonResponse } from 'src/app/modelos/pokemon-response';
import { PokemonService } from 'src/app/servicios/pokemon.service';
import { DetallePokemonComponent } from '../detalle-pokemon/detalle-pokemon.component';
import { PokemonDetalle } from 'src/app/modelos/pokemon-detalle';

@Component({
  selector: 'app-lista-pokemon',
  templateUrl: './lista-pokemon.component.html',
  styleUrls: ['./lista-pokemon.component.css']
})
export class ListaPokemonComponent implements OnInit {
  urlPokemonList = 'https://pokeapi.co/api/v2/pokemon/'
  listaPokemon: PokemonItem[] = []
  pokemonActual: PokemonDetalle = {
    abilities : [],
    forms: [],
    name: '',
  };
  anterior: string = ''
  siguiente: string = ''
  @ViewChild(DetallePokemonComponent) modal!: DetallePokemonComponent;
  constructor(private pokemonService: PokemonService) {

  }
  ngOnInit(): void {
    this.obtenerListaPokemon();
  }

  obtenerListaPokemon () {
    this.pokemonService.obtenerListaPokemon(this.urlPokemonList)
      .subscribe((data)=> {
        console.log(data)
          this.listaPokemon = data.results;
          this.anterior = data.previous;
          this.siguiente = data.next

      })
  }

  detallePokemon(url: string) {
    this.pokemonService.obtenerPokemon(url).subscribe({
      next: (data) => {
        this.pokemonActual = data ;
        console.log(data)
        
      },
      error: (err) => {
        console.error(err);
      },
      complete: () => {
        
      }
    })

    setTimeout(()=> {
      this.modal.showDialog()
  }, 2000);
  }

  clicSiguiente() {
    this.urlPokemonList = this.siguiente
    console.log(this.siguiente)
    this.obtenerListaPokemon()
  }

  clicAnterior() {
    this.urlPokemonList = this.anterior
    console.log(this.anterior)
    this.obtenerListaPokemon()
  }

  obtenerPokemon(url: string) {
    this.detallePokemon(url)
    
  }

}
